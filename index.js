const express = require('express');
const fetch = require('node-fetch');

const app = express();
const http = require('http').Server(app);

// port for localhost
const port = 4201;

app.use(function (request, response, next) {
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    response.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,DELETE');
    next();
});

// ivm CORS gaat de API van localhost naar localhost
app.get('/instruments', (request, response) => {
    const url = 'https://services.ing.nl/api/securities/mobile/markets/stockmarkets/AEX';
    fetch(url)
        .then(data => data.json())
        .then(json => response.type('json').send(json));
});

http.listen(port, () => console.log(`Listening on port ${port}`));

module.exports = app;
